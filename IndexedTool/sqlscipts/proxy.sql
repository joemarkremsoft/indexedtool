/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.45 : Database - mobwar_indexedtool
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mobwar_indexedtool` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mobwar_indexedtool`;

/*Table structure for table `proxy` */

DROP TABLE IF EXISTS `proxy`;

CREATE TABLE `proxy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `failCount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

/*Data for the table `proxy` */

insert  into `proxy`(`id`,`host`,`port`,`username`,`password`,`failCount`) values (1,'104.160.225.119',80,'kris','258025',0),(2,'107.174.237.160',80,'kris','258025',0),(3,'107.174.233.135',80,'kris','258025',0),(4,'23.94.237.78',80,'kris','258025',0),(5,'107.174.237.154',80,'kris','258025',0),(6,'155.94.192.69',80,'kris','258025',0),(7,'23.94.237.142',80,'kris','258025',0),(8,'198.46.208.23',80,'kris','258025',0),(9,'184.175.248.46',80,'kris','258025',0),(10,'198.46.208.100',80,'kris','258025',0),(11,'23.94.237.71',80,'kris','258025',0),(12,'104.160.225.191',80,'kris','258025',0),(13,'204.44.83.74',80,'kris','258025',0),(14,'181.215.147.72',80,'kris','258025',0),(15,'198.46.208.54',80,'kris','258025',0),(16,'198.46.208.77',80,'kris','258025',0),(17,'23.94.237.2',80,'kris','258025',0),(18,'192.210.201.97',80,'kris','258025',0),(19,'181.215.193.35',80,'kris','258025',0),(20,'184.175.248.149',80,'kris','258025',0),(21,'204.44.83.99',80,'kris','258025',0),(22,'23.94.237.219',80,'kris','258025',0),(23,'204.44.84.254',80,'kris','258025',0),(24,'204.44.84.216',80,'kris','258025',0),(25,'172.245.229.136',80,'kris','258025',0),(26,'198.46.208.80',80,'kris','258025',0),(27,'184.175.248.210',80,'kris','258025',0),(28,'181.215.193.39',80,'kris','258025',0),(29,'181.215.151.113',80,'kris','258025',0),(30,'69.12.65.161',80,'kris','258025',0),(31,'69.12.65.209',80,'kris','258025',0),(32,'155.94.192.113',80,'kris','258025',0),(33,'181.215.193.21',80,'kris','258025',0),(34,'107.174.237.161',80,'kris','258025',0),(35,'184.175.248.230',80,'kris','258025',0),(36,'104.160.225.172',80,'kris','258025',0),(37,'107.174.233.136',80,'kris','258025',0),(38,'104.160.225.187',80,'kris','258025',0),(39,'104.160.225.200',80,'kris','258025',0),(40,'181.215.147.93',80,'kris','258025',0),(41,'104.160.225.111',80,'kris','258025',0),(42,'104.160.225.9',80,'kris','258025',0),(43,'107.174.233.183',80,'kris','258025',0),(44,'23.94.237.33',80,'kris','258025',0),(45,'23.94.237.174',80,'kris','258025',0),(46,'198.46.208.72',80,'kris','258025',0),(47,'184.175.248.197',80,'kris','258025',0),(48,'181.215.147.91',80,'kris','258025',0),(49,'184.175.248.224',80,'kris','258025',0),(50,'184.175.248.99',80,'kris','258025',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

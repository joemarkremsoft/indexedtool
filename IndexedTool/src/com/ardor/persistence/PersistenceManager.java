package com.ardor.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * @author rmagallen
 */
public class PersistenceManager {

	private static EntityManagerFactory emf;
	private static PersistenceManager INSTANCE;

	private PersistenceManager() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("indexedtoolPU");
		}
	}

	public static synchronized PersistenceManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PersistenceManager();
		}
		return INSTANCE;
	}

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public static void closeEntityManagerFactory() {
		if (emf != null && emf.isOpen()) {
			emf.close();
		}
	}

	public static void closeEntityManager(EntityManager em) {
		if (em != null && em.isOpen()) {
			EntityTransaction et = em.getTransaction();
			if (et != null && et.isActive()) {
				et.rollback();
			}
			em.close();
		}
	}
}

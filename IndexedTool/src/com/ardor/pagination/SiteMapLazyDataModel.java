package com.ardor.pagination;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.SiteMapDAO;
import com.ardor.entity.SiteMap;
import com.ardor.model.SiteMapDataModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.SiteMapService;

public class SiteMapLazyDataModel extends LazyDataModel<SiteMapDataModel> {

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(SiteMapLazyDataModel.class);
	
	public long domainID;
	
	public SiteMapLazyDataModel(long domainID) {
		this.domainID = domainID;
	}

	@Override
	public List<SiteMapDataModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		List<SiteMapDataModel> data = new ArrayList<>();
		logger.debug("SiteMapLazyDataModel domain " + domainID);
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			SiteMapDAO siteMapDAO = new SiteMapDAO(em);
			List<SiteMap> siteMaps = siteMapDAO.findSiteMapsByDomainID(domainID, first, pageSize);
			data = new SiteMapService().buildModel(em, siteMaps);
			setRowCount(siteMapDAO.countSiteMapsByDomainID(domainID).intValue());
		} catch (Throwable e) {
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
		return data;
	}
}

package com.ardor.pagination;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.DomainDAO;
import com.ardor.entity.Domain;
import com.ardor.persistence.PersistenceManager;

public class DomainLazyDataModel extends LazyDataModel<Domain> {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(DomainLazyDataModel.class);
	
	@Override
	public List<Domain> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		List<Domain> domains = new ArrayList<>();
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			DomainDAO domainDAO = new DomainDAO(em);
			domains = domainDAO.findDomains(first, pageSize);
			setRowCount((int) domainDAO.count());
		} catch (Throwable e) {
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
		return domains;
	}
}

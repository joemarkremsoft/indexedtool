package com.ardor.exception;

public class FormValidationException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public FormValidationException(String msg) {
		super(msg);
	}
}

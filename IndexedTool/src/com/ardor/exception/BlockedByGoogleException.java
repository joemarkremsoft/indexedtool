package com.ardor.exception;

public class BlockedByGoogleException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public BlockedByGoogleException(String message) {
		super(message);
	}
}

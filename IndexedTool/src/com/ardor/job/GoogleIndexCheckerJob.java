package com.ardor.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.RandomUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.ProxyDAO;
import com.ardor.dao.SiteMapDAO;
import com.ardor.entity.Proxy;
import com.ardor.entity.SiteMap;
import com.ardor.exception.BlockedByGoogleException;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.ProxyService;
import com.ardor.service.SiteMapService;
import com.ardor.service.htmlunit.WebClientManager;
import com.ardor.service.scheduler.SchedulerService;
import com.ardor.util.GoogleIndexChecker;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.WebClient;

public class GoogleIndexCheckerJob implements Serializable, Job {
	
	private static final Logger logger = LoggerFactory.getLogger(GoogleIndexCheckerJob.class);

	private static final long serialVersionUID = 1L;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("GoogleIndexCheckerJob execute()");
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		long id = (Long) jobDataMap.get("siteMapID");
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			ProxyDAO proxyDAO = new ProxyDAO(em);
			SiteMapDAO siteMapDAO = new SiteMapDAO(em);
			
			Proxy proxy = proxyDAO.findRandom();
			SiteMap siteMap = siteMapDAO.findById(id);
			
			logger.debug("checking ... " + siteMap.getUrl());
			
			checkSiteMapToGoogle(proxy, siteMap, jobDataMap);
			
			new SiteMapService().updateSiteMap(em, siteMapDAO, siteMap);
			new ProxyService().updateProxy(em, proxyDAO, proxy);
		} catch (Throwable e) {
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}
	
	private void checkSiteMapToGoogle(Proxy proxy, SiteMap siteMap, JobDataMap jobDataMap) {
		WebClientManager webClientManager = new WebClientManager();
		webClientManager.setProxy(proxy);
		
		WebClient webClient = webClientManager.build();
		SchedulerService schedulerSevice = new SchedulerService();
		try {
			boolean isIndexed = GoogleIndexChecker.isIndexed(webClient, siteMap.getUrl());
			siteMap.setIndexed(isIndexed);
			siteMap.setLastChecked(new Date());
		} catch (BlockedByGoogleException e) {
			proxy.setFailCount(proxy.getFailCount() + 1);
			siteMap.setIndexed(null);
			schedulerSevice.scheduleSiteMapToCheckOnGoogle(siteMap, RandomUtils.nextLong(30, 120), jobDataMap);
			logger.warn("{} proxy was blocked by Google errorCount = {}", proxy.getHost(), proxy.getFailCount());
		} catch (ElementNotFoundException e) { 
			schedulerSevice.scheduleSiteMapToCheckOnGoogle(siteMap, RandomUtils.nextLong(30, 120), jobDataMap);
		} finally {
			webClient.close();
		}
	}
}

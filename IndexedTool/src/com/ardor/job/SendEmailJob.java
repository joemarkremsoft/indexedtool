package com.ardor.job;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.mail.EmailException;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.util.SendEmailUtil;

public class SendEmailJob implements Job, Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(SendEmailJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		@SuppressWarnings("unchecked")
		List<String> recipients = (List<String>) jobDataMap.get("recipients");
		String subject = (String) jobDataMap.get("subject");
		String message = (String) jobDataMap.get("message");
		String bcc = (String) jobDataMap.get("bcc");

		try {
			SendEmailUtil.send(recipients, subject, message, bcc);
		} catch (EmailException e) {
			logger.error(e.getMessage(), e);
		}
	}
}

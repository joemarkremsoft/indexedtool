package com.ardor.controller.jsf;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.SiteMapDAO;
import com.ardor.pagination.DeIndexedLazyDataModel;
import com.ardor.pagination.IndexedLazyDataModel;
import com.ardor.pagination.ProcessingLazyDataModel;
import com.ardor.pagination.SiteMapLazyDataModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.util.StringUtil;

@ManagedBean(name = "resultsSiteMapJsfController")
@ViewScoped
public class ResultsSiteMapJsfController extends AbstractJsfController {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(ResultsSiteMapJsfController.class);

	private SiteMapLazyDataModel siteMapsModel;

	private IndexedLazyDataModel indexedSiteMapsModel;

	private DeIndexedLazyDataModel deIndexedSiteMapsModel;

	private ProcessingLazyDataModel processingSiteMapsModel;

	private Map<String, String> paramMap;

	private int indexedCount;

	private int deIndexedCount;

	private int processingCount;

	public ResultsSiteMapJsfController() {
		paramMap = getParameterMap();
		long domainID = StringUtil.stringToLong(paramMap.get("domainID"), 0L);
		siteMapsModel = new SiteMapLazyDataModel(domainID);
		indexedSiteMapsModel = new IndexedLazyDataModel(domainID);
		deIndexedSiteMapsModel = new DeIndexedLazyDataModel(domainID);
		processingSiteMapsModel = new ProcessingLazyDataModel(domainID);
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			SiteMapDAO siteMapDAO = new SiteMapDAO(em);
			indexedCount = siteMapDAO.countSiteMapsByDomainID(true, domainID).intValue();
			deIndexedCount = siteMapDAO.countSiteMapsByDomainID(false, domainID).intValue();
			processingCount = siteMapDAO.countSiteMapsByDomainID(null, domainID).intValue();
		} catch (Throwable e) {
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}

	public SiteMapLazyDataModel getSiteMapsModel() {
		return siteMapsModel;
	}

	public Map<String, String> getParamMap() {
		return paramMap;
	}

	public int getIndexedCount() {
		return indexedCount;
	}

	public int getDeIndexedCount() {
		return deIndexedCount;
	}

	public int getProcessingCount() {
		return processingCount;
	}

	public IndexedLazyDataModel getIndexedSiteMapsModel() {
		return indexedSiteMapsModel;
	}

	public DeIndexedLazyDataModel getDeIndexedSiteMapsModel() {
		return deIndexedSiteMapsModel;
	}

	public ProcessingLazyDataModel getProcessingSiteMapsModel() {
		return processingSiteMapsModel;
	}
}

package com.ardor.controller.jsf;

import java.io.IOException;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.entity.Domain;
import com.ardor.exception.FormValidationException;
import com.ardor.pagination.DomainLazyDataModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.DomainService;
import com.ardor.service.SiteMapService;
import com.ardor.util.DomainUtil;
import com.ardor.util.FormValidationUtil;
import com.ardor.util.SiteMapUtil;

import crawlercommons.sitemaps.UnknownFormatException;

@ManagedBean(name = "inputSiteMapJsfController")
@ViewScoped
public class InputSiteMapJsfController extends AbstractJsfController {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = LoggerFactory.getLogger(InputSiteMapJsfController.class);
	
	private DomainLazyDataModel domains;
	
	public InputSiteMapJsfController() {
		domains = new DomainLazyDataModel();
	}
	
	public void doAnalyze() {
		String siteMapUrlIndex = text.get("url").trim();
		logger.debug("Parsing " + siteMapUrlIndex);
		
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			FormValidationUtil.validateURL(siteMapUrlIndex);
			
			List<String> siteMapURLs = SiteMapUtil.collectSiteMapURLs(siteMapUrlIndex);
			
			if (siteMapURLs.size() == 0) {
				addErrorMessage("Total of " + siteMapURLs.size() + " site map found. Please provide valid site map url.");
				return;
			}
			
			String domainName = DomainUtil.extracDomainName(siteMapUrlIndex);
			Domain domain = new DomainService().createDomain(em, domainName);
			
			SiteMapService siteMapService = new SiteMapService();
			siteMapService.saveAllSiteMaps(em, siteMapURLs, domain);
			siteMapService.checkSiteMapsToGoogleByDomainName(domainName);
			
			addInfoMessage("Total of " + siteMapURLs.size() + " site maps found.");
		} catch (IOException | UnknownFormatException | FormValidationException e) {
			logger.warn(e.getMessage(), e);
			addErrorMessage(e.getMessage());
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}

	public DomainLazyDataModel getDomains() {
		return domains;
	}

	public void setDomains(DomainLazyDataModel domains) {
		this.domains = domains;
	}
}

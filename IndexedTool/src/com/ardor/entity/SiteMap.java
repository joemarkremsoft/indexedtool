package com.ardor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "sitemap")
public class SiteMap implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id;
	
	private String url;
	
	@Column(columnDefinition = "bit")
	private Boolean isIndexed;
	
	@ManyToOne
	@JoinColumn(name = "domainID")
	private Domain domain;
	
	private Date dateCreated;
	
	private Date lastChecked;
	
	@SuppressWarnings("unused")
	private SiteMap() {
		
	}
	
	public SiteMap(String url) {
		this.url = url;
	}

	public long getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean isIndexed() {
		return isIndexed;
	}

	public void setIndexed(Boolean isIndexed) {
		this.isIndexed = isIndexed;
	}
	
	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastChecked() {
		return lastChecked;
	}

	public void setLastChecked(Date lastChecked) {
		this.lastChecked = lastChecked;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass() != getClass()) {
			return false;
		}
		
		SiteMap o = (SiteMap) object;
		return new EqualsBuilder().append(id, o.getId()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31).append(id).toHashCode();
	}
	
	@Override
	public String toString() {
		return "[id = " + id + " url = '"+ url +"']";
	}
}

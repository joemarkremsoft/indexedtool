package com.ardor.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "domain")
public class Domain implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private long id;
	
	private String domainName;
	
	@SuppressWarnings("unused")
	private Domain() {
		
	}
	
	public Domain(String domainName) {
		this.domainName = domainName;
	}

	public long getId() {
		return id;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass() != getClass()) {
			return false;
		}
		
		Domain o = (Domain) object;
		return new EqualsBuilder().append(id, o.getId()).append(this.domainName, o.domainName).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31).append(id).toHashCode();
	}
	
	@Override
	public String toString() {
		return "[id = " + id + " domain = '"+ domainName +"']";
	}
}

package com.ardor.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 * @author rmagallen
 * @param <T>
 */
public abstract class AbstractDAO<T extends Serializable> {

	protected EntityManager em;
	protected Class<?> entityClass;

	public AbstractDAO(EntityManager em, Class<?> c) {
		this.em = em;
		this.entityClass = c;
	}

	@SuppressWarnings("unchecked")
	protected T getSingleResult(Query query) {
		List<T> entityList = query.getResultList();
		return entityList.size() > 0 ? entityList.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	protected List<T> getResultList(Query query) {
		return query.getResultList();
	}

	public List<T> findAll() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM ").append(entityClass.getSimpleName()).append(" e ");
		Query query = em.createQuery(sql.toString());
		return getResultList(query);
	}

	public List<T> findAll(String orderBy) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM ").append(entityClass.getSimpleName()).append(" e ").append(orderBy);
		Query query = em.createQuery(sql.toString());
		return getResultList(query);
	}

	public List<T> findAll(String orderBy, int firstResult, int maxResult) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM ").append(entityClass.getSimpleName()).append(" e ").append(orderBy);
		Query query = em.createQuery(sql.toString());
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		return getResultList(query);
	}

	public List<T> findAll(int firstResult, int maxResult) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM ").append(entityClass.getSimpleName()).append(" e ");
		Query query = em.createQuery(sql.toString());
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		return getResultList(query);
	}

	@SuppressWarnings("unchecked")
	public T findById(Serializable id) {
		if (id == null) {
			throw new PersistenceException("id is null");
		}
		return (T) em.find(entityClass, id);
	}

	public long count() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(e) FROM ").append(entityClass.getSimpleName()).append(" e ");
		Query query = em.createQuery(sql.toString());
		return (long) query.getSingleResult();
	}

	public T create(T entity) {
		em.persist(entity);
		return entity;
	}

	public T update(T entity) {
		return em.merge(entity);
	}

	public boolean delete(Serializable id) {
		T entity = findById(id);
		if (entity != null) {
			em.remove(entity);
			return true;
		}
		return false;
	}

	public void refresh(T entity) {
		em.refresh(em.merge(entity));
	}
}

package com.ardor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ardor.entity.SiteMap;

public class SiteMapDAO extends AbstractDAO<SiteMap> {

	public SiteMapDAO(EntityManager em) {
		super(em, SiteMap.class);
	}
	
	public List<SiteMap> findSiteMapsByDomainName(String domainName, int firstResult, int maxResult) {
		String sql = "SELECT e FROM SiteMap e WHERE e.url LIKE :domainName ORDER BY e.id DESC";
		Query query = em.createQuery(sql);
		query.setParameter("domainName", "%" + domainName + "%");
		query.setMaxResults(maxResult);
		query.setFirstResult(firstResult);

		return getResultList(query);
	}
	
	public List<SiteMap> findSiteMapsByDomainID(long domainID, int firstResult, int maxResult) {
		String sql = "SELECT e FROM SiteMap e WHERE e.domain.id = :domainID ORDER BY e.isIndexed DESC";
		Query query = em.createQuery(sql);
		query.setParameter("domainID", domainID);
		query.setMaxResults(maxResult);
		query.setFirstResult(firstResult);

		return getResultList(query);
	}
	
	public List<SiteMap> findSiteMapsByDomainID(Boolean isIndexed, long domainID, int firstResult, int maxResult) {
		String sql = "SELECT e FROM SiteMap e WHERE e.domain.id = :domainID AND e.isIndexed = :isIndexed";
		Query query = em.createQuery(sql);
		query.setParameter("domainID", domainID);
		query.setParameter("isIndexed", isIndexed);
		query.setMaxResults(maxResult);
		query.setFirstResult(firstResult);

		return getResultList(query);
	}
	
	public Long countSiteMapsByDomainName(String domainName) {
		String sql = "SELECT COUNT(e) FROM SiteMap e WHERE e.url LIKE :domainName";
		Query query = em.createQuery(sql);
		query.setParameter("domainName", "%" + domainName + "%");
		return (Long) query.getSingleResult();
	}
	
	public Long countSiteMapsByDomainID(long domainID) {
		String sql = "SELECT COUNT(e) FROM SiteMap e WHERE e.domain.id = :domainID";
		Query query = em.createQuery(sql);
		query.setParameter("domainID", domainID);
		return (Long) query.getSingleResult();
	}
	
	public Long countIndexedSiteMapsByDomainName(String domainName) {
		String sql = "SELECT COUNT(e) FROM SiteMap e WHERE e.url LIKE :domainName AND e.isIndexed = TRUE";
		Query query = em.createQuery(sql);
		query.setParameter("domainName", "%" + domainName + "%");
		return (Long) query.getSingleResult();
	}
	
	public Long countDeIndexedSiteMapsByDomainName(String domainName) {
		String sql = "SELECT COUNT(e) FROM SiteMap e WHERE e.url LIKE :domainName AND e.isIndexed = FALSE";
		Query query = em.createQuery(sql);
		query.setParameter("domainName", "%" + domainName + "%");
		return (Long) query.getSingleResult();
	}
	
	public Long countSiteMapsByDomainID(Boolean isIndexed, long domainID) {
		String sql = "SELECT COUNT(e) FROM SiteMap e WHERE e.domain.id = :domainID AND e.isIndexed = :isIndexed";
		Query query = em.createQuery(sql);
		query.setParameter("domainID", domainID);
		query.setParameter("isIndexed", isIndexed);
		return (Long) query.getSingleResult();
	}
	
	public SiteMap findByURL(String url) {
		String sql = "SELECT e FROM SiteMap e WHERE e.url = :url";
		Query query = em.createQuery(sql);
		query.setParameter("url", url);
		return getSingleResult(query);
	}
}

package com.ardor.dao;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ardor.entity.Proxy;

public class ProxyDAO extends AbstractDAO<Proxy> {

	public ProxyDAO(EntityManager em) {
		super(em, Proxy.class);
	}

	public Proxy findRandom() {
		String sql = "SELECT * FROM proxy WHERE failCount < 3 ORDER BY RAND() LIMIT 1";
		Query query = em.createNativeQuery(sql, Proxy.class);
		return getSingleResult(query);
	}

	public void deleteAll() {
		String sql = "DELETE FROM Proxy";
		Query query = em.createQuery(sql);
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<BigInteger> findIDsInRandomOrder() {
		String sql = "SELECT e.id FROM proxy e ORDER BY RAND()";
		Query query = em.createNativeQuery(sql);
		return query.getResultList();
	}
}

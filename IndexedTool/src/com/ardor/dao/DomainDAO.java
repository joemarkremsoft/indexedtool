package com.ardor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.ardor.entity.Domain;

public class DomainDAO extends AbstractDAO<Domain> {

	public DomainDAO(EntityManager em) {
		super(em, Domain.class);
	}
	
	public Domain findByDomainName(String domainName) {
		String sql = "SELECT e FROM Domain e WHERE e.domainName = :domainName";
		Query query = em.createQuery(sql);
		query.setParameter("domainName", "%" + domainName + "%");
		return getSingleResult(query);
	}
	
	public List<Domain> findDomains(int firstResult, int maxResult) {
		String sql = "SELECT e FROM Domain e";
		Query query = em.createQuery(sql);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		return getResultList(query);
	}
}

package com.ardor.model;

import com.ardor.entity.SiteMap;

public class SiteMapDataModel {
	
	private String url;
	
	private String status;
	
	public SiteMapDataModel(SiteMap siteMap) {
		this.url = siteMap.getUrl();
		this.status = siteMap.isIndexed() == null ? "Processing" : siteMap.isIndexed() ? "Yes" : "No"; 
	}

	public String getUrl() {
		return url;
	}
	
	public String getStatus() {
		return status;
	}
}

package com.ardor.service.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.ObjectAlreadyExistsException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractSchedulerService implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AbstractSchedulerService.class);

	// for scheduling non-repeating jobs
	protected void scheduleJob(Class<? extends Job> clazz, Date triggerStartTime, JobDataMap jobDataMap, String jobName, String jobGroup) {

		JobDetail jobDetail = newJob(clazz).withIdentity(jobName, jobGroup).build();
		jobDetail.getJobDataMap().putAll(jobDataMap);

		Trigger trigger = newTrigger().withIdentity(jobName, jobGroup).usingJobData(jobDataMap).startAt(triggerStartTime).withSchedule(simpleSchedule().withMisfireHandlingInstructionIgnoreMisfires()).build();

		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			if (scheduler.checkExists(jobDetail.getKey())) {
				scheduler.deleteJob(jobDetail.getKey());
			}
			
			scheduler.scheduleJob(jobDetail, trigger);
			
		} catch (ObjectAlreadyExistsException e) {
			scheduleJob(clazz, triggerStartTime, jobDataMap, jobName + RandomStringUtils.randomAlphanumeric(12), jobGroup); //if job already exists modify the job name and try again
		} catch (SchedulerException se) {
			logger.error(se.getMessage(), se);
		}
	}

	// for scheduling repeating jobs, see http://www.cronmaker.com/ for building triggerCronExpr
	protected void scheduleCronJob(Class<? extends Job> clazz, String triggerCronExpr, JobDataMap jobDataMap, String jobName, String jobGroup) {
		JobDetail jobDetail = newJob(clazz).withIdentity(jobName, jobGroup).build();
		jobDetail.getJobDataMap().putAll(jobDataMap);

		Trigger trigger = newTrigger().withIdentity(jobName, jobGroup).usingJobData(jobDataMap).withSchedule(cronSchedule(triggerCronExpr).withMisfireHandlingInstructionIgnoreMisfires()).build();

		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			if (scheduler.checkExists(jobDetail.getKey())) {
				scheduler.deleteJob(jobDetail.getKey());
			}
			scheduler.scheduleJob(jobDetail, trigger);

			logger.info("Scheduled {}\n{}", jobDetail.getKey().toString(), ((CronTrigger) trigger).getExpressionSummary());
		} catch (SchedulerException se) {
			logger.error(se.getMessage(), se);
		}
	}

	public void deleteAllJobs() {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.clear();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void shutDown() {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			boolean waitForJobsToComplete = false;
			scheduler.shutdown(waitForJobsToComplete);
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
	}

}

package com.ardor.service.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.RandomUtils;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.Const;
import com.ardor.entity.Domain;
import com.ardor.entity.SiteMap;
import com.ardor.job.GoogleIndexCheckerJob;
import com.ardor.job.SendEmailJob;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.DomainService;
import com.ardor.service.SiteMapService;
import com.ardor.util.DomainUtil;
import com.ardor.util.SiteMapUtil;

import crawlercommons.sitemaps.UnknownFormatException;

public class SchedulerService extends AbstractSchedulerService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

	private static final long serialVersionUID = 1L;

	private static final String EMAIL_JOB_GROUP = "EMAIL_JOB_GROUP";

	private static final String GOOGLE_CHECKER_GROUP = "GOOBLE_CHECKER_GROUP";

	// for sending email to single recipient
	public void scheduleSendEmail(String recipient, String subject, String message, String bcc) {
		List<String> recipients = new ArrayList<String>();
		recipients.add(recipient);
		scheduleSendEmail(recipients, subject, message, bcc);
	}

	// for sending email to multiple recipients, runs 3 seconds after method is
	// called
	public void scheduleSendEmail(List<String> recipients, String subject, String message, String bcc) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("recipients", recipients);
		jobDataMap.put("subject", subject);
		jobDataMap.put("message", message);
		jobDataMap.put("bcc", bcc);

		String jobName = SendEmailJob.class.getSimpleName() + System.nanoTime();
		Date triggerStartTime = new Date(System.currentTimeMillis() + (3 * Const.SECOND_IN_MILLISECONDS));
		scheduleJob(SendEmailJob.class, triggerStartTime, jobDataMap, jobName, EMAIL_JOB_GROUP);
	}

	public void scheduleGoogleIndexedChecker(List<SiteMap> siteMaps, long interval) throws InterruptedException {
		JobDataMap jobDataMap = new JobDataMap();
		
		for (SiteMap site : siteMaps) {
			// System.out.println(site.toString());
			jobDataMap.put("siteMapID", site.getId());
			scheduleSiteMapToCheckOnGoogle(site, interval, jobDataMap);
		}
	}
	
	public void scheduleSiteMapToCheckOnGoogle(SiteMap site, long interval, JobDataMap jobDataMap) {
		String jobName = GoogleIndexCheckerJob.class.getSimpleName() + System.nanoTime();
		Date triggerStartTime = new Date(System.currentTimeMillis() + ( (RandomUtils.nextLong(15, 180) + interval) * Const.SECOND_IN_MILLISECONDS));
		scheduleJob(GoogleIndexCheckerJob.class, triggerStartTime, jobDataMap, jobName, GOOGLE_CHECKER_GROUP);
	}
	
	public void doTest() throws IOException, UnknownFormatException {
		String url = "https://ardorseo.com/page-sitemap.xml";
		// Step 1: parse all site maps
		List<String> siteMapUrls = SiteMapUtil.collectSiteMapURLs(url);
		String domainName = DomainUtil.extracDomainName(url);
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			
			Domain domain = new DomainService().createDomain(em, domainName);
			new SiteMapService().saveAllSiteMaps(em, siteMapUrls, domain);
			
			// Step 4: schedule all sitemaps persisted in db based on domain name 
			// 		   to check on Google if indexed or not
			new SiteMapService().checkSiteMapsToGoogleByDomainName(domainName);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}
}

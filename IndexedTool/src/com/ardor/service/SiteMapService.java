package com.ardor.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.dao.SiteMapDAO;
import com.ardor.entity.Domain;
import com.ardor.entity.SiteMap;
import com.ardor.model.SiteMapDataModel;
import com.ardor.persistence.PersistenceManager;
import com.ardor.service.scheduler.SchedulerService;

public class SiteMapService {
	
	private static final Logger logger = LoggerFactory.getLogger(SiteMapService.class);
	
	public void checkSiteMapsToGoogleByDomainName(String domainName) {
		final int MAX = 50;
		EntityManager em = PersistenceManager.getInstance().getEntityManager();
		try {
			SchedulerService schedulerService = new SchedulerService();
			SiteMapDAO  sitemMapDAO = new SiteMapDAO(em);
			
			long totalSiteMapsFound = sitemMapDAO.countSiteMapsByDomainName(domainName);
			for (int i = 0, counter = 0, start = 0; i < totalSiteMapsFound && counter <= totalSiteMapsFound; i++) {
				List<SiteMap> siteMaps = sitemMapDAO.findSiteMapsByDomainName(domainName, start, MAX);
				counter += MAX;
				start   += siteMaps.size();
				logger.debug("SiteMapsFound = " + totalSiteMapsFound + " counter = " + counter + " siteMapSize = " + siteMaps.size());
				schedulerService.scheduleGoogleIndexedChecker(siteMaps, RandomUtils.nextLong(90, 180));
			}
		} catch (Throwable e) {
			logger.warn(e.getMessage(), e);
		} finally {
			PersistenceManager.closeEntityManager(em);
		}
	}
			
	public void saveAllSiteMaps(EntityManager em, List<String> siteMapUrls, Domain domain) {
		SiteMapDAO siteMapDAO = new SiteMapDAO(em);
		EntityTransaction et = em.getTransaction();
		et.begin();
		for (String url : siteMapUrls) {
			if (isSiteMapNotExist(em, siteMapDAO, url)) {
				SiteMap siteMap = new SiteMap(url);
				siteMap.setDateCreated(new Date());
				siteMap.setDomain(domain);
				siteMapDAO.create(siteMap);
				em.flush();
				em.clear();
			}
		}
		et.commit();
	}
	
	public void createSiteMap(EntityManager em, SiteMapDAO siteMapDAO, String url, Domain domain) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		SiteMap siteMap = new SiteMap(url);
		siteMap.setDateCreated(new Date());
		siteMap.setDomain(domain);
		siteMapDAO.create(siteMap);
		em.flush();
		em.clear();
		et.commit();
	}
	
	public void createSiteMap(EntityManager em, String url) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		SiteMap siteMap = new SiteMap(url);
		siteMap.setDateCreated(new Date());
		new SiteMapDAO(em).create(siteMap);
		em.flush();
		em.clear();
		et.commit();
	}
	
	public void updateSiteMap(EntityManager em, SiteMapDAO siteMapDAO, SiteMap siteMap) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		siteMapDAO.update(siteMap);
		em.flush();
		em.clear();
		et.commit();
	}
	
	public void updateSiteMap(EntityManager em, SiteMap siteMap) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		new SiteMapDAO(em).update(siteMap);
		em.flush();
		em.clear();
		et.commit();
	}
	
	public boolean isSiteMapNotExist(EntityManager em, String url) {
		SiteMap siteMap = new SiteMapDAO(em).findByURL(url);
		return siteMap == null ? true : false;
	}
	
	public boolean isSiteMapNotExist(EntityManager em, SiteMapDAO siteMapDAO, String url) {
		SiteMap siteMap = siteMapDAO.findByURL(url);
		return siteMap == null ? true : false;
	}

	public List<SiteMapDataModel> buildModel(EntityManager em, List<SiteMap> siteMaps) {
		List<SiteMapDataModel> siteMapsDataModel = new ArrayList<>();
		for (SiteMap siteMap : siteMaps) {
			siteMapsDataModel.add( new SiteMapDataModel(siteMap));
		}
		return siteMapsDataModel;
	}
}

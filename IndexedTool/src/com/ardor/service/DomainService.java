package com.ardor.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.ardor.dao.DomainDAO;
import com.ardor.entity.Domain;

public class DomainService {
	
	public Domain createDomain(EntityManager em, String domainName) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		Domain domainEntity = new DomainDAO(em).create(new Domain(domainName));
		em.flush();
		em.clear();
		et.commit();
		
		return domainEntity;
	}
	
	public boolean isDomainExist(EntityManager em, String domainName) {
		Domain domain = new DomainDAO(em).findByDomainName(domainName);
		return domain != null ? true : false;
	}
}

package com.ardor.service.htmlunit;

import java.net.URL;

import com.gargoylesoftware.htmlunit.html.HTMLParserListener;

public class SilentHTMLParserListener implements HTMLParserListener {

	@Override
	public void error(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
		//ignore
	}

	@Override
	public void warning(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
		//ignore
	}
}

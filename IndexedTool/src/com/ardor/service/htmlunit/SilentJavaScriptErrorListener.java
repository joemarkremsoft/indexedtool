package com.ardor.service.htmlunit;

import java.net.MalformedURLException;
import java.net.URL;

import com.gargoylesoftware.htmlunit.InteractivePage;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;

public class SilentJavaScriptErrorListener implements JavaScriptErrorListener {

	@Override
	public void loadScriptError(InteractivePage arg0, URL arg1, Exception arg2) {
		// ignore
	}

	@Override
	public void malformedScriptURL(InteractivePage arg0, String arg1, MalformedURLException arg2) {
		// ignore
	}

	@Override
	public void scriptException(InteractivePage arg0, ScriptException arg1) {
		// ignore
	}

	@Override
	public void timeoutError(InteractivePage arg0, long arg1, long arg2) {
		// ignore
	}
}

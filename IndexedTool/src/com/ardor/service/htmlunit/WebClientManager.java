package com.ardor.service.htmlunit;

import java.io.IOException;
import java.net.URL;

import com.ardor.entity.Proxy;
import com.ardor.exception.BlockedByGoogleException;
import com.ardor.util.GoogleIndexChecker;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.RefreshHandler;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;

public class WebClientManager {

	private static final int JAVASCRIPT_TIMEOUT = 6 * 1000;
	private static final int TIMEOUT = 60 * 1000;

	private BrowserVersion browserVersion;
	private Proxy proxy;

	static {
		System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
	}

	public void setBrowserVersion(BrowserVersion browserVersion) {
		this.browserVersion = browserVersion;
	}

	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	public WebClient build() {
		if (browserVersion == null) {
			browserVersion = BrowserVersion.CHROME;
		}

		WebClient webClient = proxy == null ? new WebClient(browserVersion)
				: new WebClient(browserVersion, proxy.getHost(), proxy.getPort());

		if (proxy != null) {
			final DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient
					.getCredentialsProvider();
			credentialsProvider.addCredentials(proxy.getUsername(), proxy.getPassword());
		}

		webClient.setCssErrorHandler(new SilentCssErrorHandler());
		webClient.setHTMLParserListener(new SilentHTMLParserListener());
		webClient.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());
		webClient.setCssErrorHandler(new SilentCssErrorHandler());
		webClient.waitForBackgroundJavaScript(JAVASCRIPT_TIMEOUT);
		webClient.setJavaScriptTimeout(JAVASCRIPT_TIMEOUT);

		RefreshHandler refreshHandler = new RefreshHandler() {
			@Override
			public void handleRefresh(Page page, URL url, int seconds) throws IOException {
				// do nothing
			}
		};
		webClient.setRefreshHandler(refreshHandler);

		webClient.getCookieManager().setCookiesEnabled(false);

		WebClientOptions options = webClient.getOptions();
		options.setThrowExceptionOnFailingStatusCode(false);
		options.setThrowExceptionOnScriptError(false);
		options.setPrintContentOnFailingStatusCode(false);
		options.setUseInsecureSSL(true);
		options.setRedirectEnabled(true);
		options.setPopupBlockerEnabled(true);
		options.setTimeout(TIMEOUT);

		options.setActiveXNative(false);
		options.setAppletEnabled(false);
		options.setCssEnabled(false);
		options.setJavaScriptEnabled(false);

		return webClient;
	}

	public static void main(String[] args) throws BlockedByGoogleException {
		String url = "http://www.billard-5962.com/";
		WebClient webClient = new WebClientManager().build();
		try {
			System.out.println(GoogleIndexChecker.isIndexed(webClient, url));
		} finally {
			webClient.close();
			System.gc();
		}
	}
}

package com.ardor.service.htmlunit;

import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.util.SleepUtil;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class WebClientHelper implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(WebClientHelper.class);
	
	public static HtmlPage getHtmlPage(WebClient webClient, String url) {
		try {
			Page page = webClient.getPage(url);

			if (page != null) {
				SleepUtil.sleep(1000 * 20);

				if (page.isHtmlPage()) {
					return (HtmlPage) page;
				} else {
					logger.debug("page.isHtmlPage() returned false ({})", page.getWebResponse().getContentType());
				}
			}
		} catch (FailingHttpStatusCodeException | IOException e) {
			logger.error("getHtmlPage('{}') encountered an error! {}", url, e.getMessage());
		}
		return null;
	}

	public static String getContentString(WebClient webClient, String url) {
		try {
			Page page = webClient.getPage(url);

			if (page != null) {
				SleepUtil.sleep(1000 * 20);
				return page.getWebResponse().getContentAsString();
			}
		} catch (FailingHttpStatusCodeException | IOException e) {
			logger.error("getContentString('{}') encountered an error! {}", url, e.getMessage());
		}
		return null;
	}

	public static String getContentString(WebClient webClient, WebRequest webRequest) {
		try {
			Page page = webClient.getPage(webRequest);

			if (page != null) {
				SleepUtil.sleep(1000 * 20);
				return page.getWebResponse().getContentAsString();
			}
		} catch (FailingHttpStatusCodeException | IOException e) {
			logger.error("getContentString('{}') encountered an error! {}", webRequest, e.getMessage());
		}
		return null;
	}

	public static int getHttpStatusCode(HtmlPage htmlPage) {
		try {
			return htmlPage.getWebResponse().getStatusCode();
		} catch (FailingHttpStatusCodeException | IllegalArgumentException e) {
			logger.error("getHttpStatusCode() encountered an error! {}", e.getMessage());
		}
		return -1;
	}

}

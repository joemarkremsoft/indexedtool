package com.ardor.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.ardor.dao.ProxyDAO;
import com.ardor.entity.Proxy;

public class ProxyService {
	
	public void updateProxy(EntityManager em, ProxyDAO proxyDAO, Proxy proxy) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		proxyDAO.update(proxy);
		em.flush();
		em.clear();
		et.commit();
	}
	
	public void updateProxy(EntityManager em, Proxy proxy) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		new ProxyDAO(em).update(proxy);
		em.flush();
		em.clear();
		et.commit();
	}
}

package com.ardor.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SleepUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(SleepUtil.class);
	
	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			logger.warn(e.getMessage(), e);
		}
	}

}

package com.ardor.util;

import com.ardor.exception.FormValidationException;

public class FormValidationUtil {
	
	public static void validateURL(String url) throws FormValidationException {
		if (url.isEmpty()) {
			throw new FormValidationException("Sitem map URL must not be empty");
		}
	}
}

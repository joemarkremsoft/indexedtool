package com.ardor.util;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ardor.exception.BlockedByGoogleException;
import com.ardor.service.htmlunit.WebClientHelper;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

public class GoogleIndexChecker {

	private static final Logger logger = LoggerFactory.getLogger(GoogleIndexChecker.class);

	private static final String GOOGLE_SITE = "https://www.google.com";

	public static boolean isIndexed(WebClient webClient, String siteMapUrl) throws BlockedByGoogleException {
		boolean isSiteMapIndexedByGoogle = false;
		try {
			HtmlPage htmlPage = WebClientHelper.getHtmlPage(webClient, GOOGLE_SITE);
			if (htmlPage != null) {
				HtmlInput search = htmlPage.getElementByName("q");
				search.setValueAttribute("site:" + siteMapUrl);
	
				if (htmlPage.getElementByName("btnK") instanceof HtmlSubmitInput) {
					HtmlSubmitInput submit = htmlPage.getElementByName("btnK");
					htmlPage = submit.click();
				} else {
					HtmlButton submit = htmlPage.getElementByName("btnK");
					htmlPage = submit.click();
				}
				SleepUtil.sleep(1000 * 4);
				isSiteMapIndexedByGoogle = isGoogleHasResults(htmlPage, siteMapUrl);
			}
		} catch (FailingHttpStatusCodeException | IOException e) {
			logger.warn("isIndex() {} {}", siteMapUrl, e);
		}
		return isSiteMapIndexedByGoogle;
	}

	private static boolean isGoogleHasResults(HtmlPage htmlPage, String url) throws BlockedByGoogleException {
		if (htmlPage.getWebResponse().getStatusCode() != HttpStatus.SC_OK) {
			throw new BlockedByGoogleException("IP was Blocked By Google");
		}
		
		for (DomElement element : htmlPage.getElementsByTagName("h3")) {
			if (element.hasAttribute("class") && element.getAttribute("class").equals("r")) {
				String link = element.getElementsByTagName("a").get(0).getAttribute("href");
				if (link.contains(url)) {
					return true;
				}
			}
		}
		return false;
	}
}

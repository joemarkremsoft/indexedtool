package com.ardor.util;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crawlercommons.sitemaps.AbstractSiteMap;
import crawlercommons.sitemaps.SiteMap;
import crawlercommons.sitemaps.SiteMapIndex;
import crawlercommons.sitemaps.SiteMapParser;
import crawlercommons.sitemaps.SiteMapURL;
import crawlercommons.sitemaps.UnknownFormatException;

public class SiteMapUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(SiteMapUtil.class);
	
	private static SiteMapParser parser = new SiteMapParser(false);
	
	public static List<String> collectSiteMapURLs(String siteMapUrlIndex) throws IOException, UnknownFormatException {
		List<String> siteMapURLs = new LinkedList<>();
		
		URL url = new URL(siteMapUrlIndex);
		String mediaType = null;
		parse(url, mediaType, siteMapURLs);
		
		return siteMapURLs;
	}
	
	/**
	 * Parses a Sitemap recursively meaning that if the sitemap is a sitemapIndex
	 * then it parses all of the internal sitemaps
	 * @throws UnknownFormatException 
	 */
	private static void parse(URL url, String mt, List<String> siteMapURLs) throws IOException, UnknownFormatException {
		byte[] content = IOUtils.toByteArray(url);

		AbstractSiteMap sm = null;
		// guesses the mimetype
		if (mt == null || mt.equals("")) {
			sm = parser.parseSiteMap(url);
			
		} else {
			sm = parser.parseSiteMap(mt, content, url);
		}

		if (sm.isIndex()) {
			Collection<AbstractSiteMap> links = ((SiteMapIndex) sm).getSitemaps();
			for (AbstractSiteMap asm : links) {
				parse(asm.getUrl(), mt, siteMapURLs); // Recursive call
			}
		} else {
			Collection<SiteMapURL> links = ((SiteMap) sm).getSiteMapUrls();
			for (SiteMapURL smu : links) {
				logger.debug("parsing..." + smu.getUrl().toString());
				siteMapURLs.add(smu.getUrl().toString());
			}
		}
	}
}

package com.ardor.util;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.InternetDomainName;

public class DomainUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(DomainUtil.class);
			
	public static String extracDomainName(String url) {
		try {
			URI uri = new URI(url);
			if (url != null && uri.getHost() != null) {
				return InternetDomainName.from(uri.getHost()).topPrivateDomain().toString();
				
			}
		} catch (Exception e) {
			logger.warn("getDomainName() encountered an error! {}", e.getMessage());
		}
		return StringUtil.EMPTY_STRING;
	}
}
